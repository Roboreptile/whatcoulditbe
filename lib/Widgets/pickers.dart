import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';

import '../Screens/create_screen.dart';
import '../constants.dart';
import '../main.dart';



class ColorPicker extends StatefulWidget{
  final CreateState _state;
  ColorPicker(this._state);

  @override
  ColorPickerState createState() => ColorPickerState(_state);
}

class ColorPickerState extends State<ColorPicker>{
  final CreateState _state;
  ColorPickerState(this._state);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
            children: [
              customButton(Colors.red),
              customButton(Colors.green),
              customButton(Colors.blue),
              customButton(Colors.purpleAccent),
            ]
        ),
        Row(
          children: [
            customButton(Colors_claret),
            customButton(Colors.black),
            customButton(Colors.yellow),
            customButton(Colors.orange)
          ],
        )
      ],
    );
  }

  Widget customButton(Color c){
    return FlatButton(
        height: 20,
        minWidth: 20,
        onPressed: (){
          if(MyApp.vibrations) Vibration.vibrate(duration: 50);
          setState(() {
          CreateState.allColor = c;
          _state.reload();
        });},
        child: ClipOval(
          child: Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
                color: equalColor(CreateState.allColor, c) ? c : c.withOpacity(0.5)
            ),
            child: equalColor(CreateState.allColor, c) ? Icon(Icons.favorite, size:10, color: Colors_max_yellow_red) : null,
          ),
        )
    );
  }
  bool equalColor(Color c1, Color c2){
    return c1.red == c2.red && c1.blue == c2.blue && c1.green == c2.green;
  }
}

class GifPicker extends StatefulWidget{
  final CreateState _state;
  GifPicker(this._state);
  
  @override
  GifPickerState createState() => GifPickerState(_state);
}

class GifPickerState extends State<GifPicker>{
  final CreateState _state;
  GifPickerState(this._state);
  
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('crab'),
              gifBox('fit'),
              gifBox('book'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('donut'),
              gifBox('hearts'),
              gifBox('money'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('pizza'),
              gifBox('funny'),
              gifBox('glasses'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('fire'),
              gifBox('pony'),
              gifBox('music'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('creative'),
              gifBox('rabbit'),
              gifBox('dawn'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('dance'),
              gifBox('popheart'),
              gifBox('kiss'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('parrot'),
              gifBox('duck'),
              gifBox('banana'),
            ]
        ),

        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('kitty'),
              gifBox('panda'),
              gifBox('puppy'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('bath'),
              gifBox('fries'),
              gifBox('drink'),
            ]
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              gifBox('sunny'),
              gifBox('xmas'),
              gifBox('sport'),
            ]
        ),
        Container(
          height: 50,
          width: 1,
        )
      ]
    );
  }
  Widget gifBox(String _name) {
    return GestureDetector(
        onTap: () => {
          if(MyApp.vibrations) Vibration.vibrate(duration: 50),
          CreateState.gifName = _name,
          _state.reload(),
        },
        child: Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                border: Border.all(color: CreateState.allColor, width: 2),
                image: DecorationImage(
                    image: AssetImage(gifPath + _name + ".gif"),
                    fit: BoxFit.fill
                )
            ),
            child: CreateState.gifName == _name ? Icon(
                Icons.favorite, size: 30, color: Colors_max_yellow_red) : null
        )
    );
  }
}