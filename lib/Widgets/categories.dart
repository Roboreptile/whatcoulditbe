import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';


import '../Screens/gallery_screen.dart';
import '../constants.dart';
import '../main.dart';
import 'categories_loader.dart';

class Categories{
  static int howMany = 3;
  static List<Widget> categories = [];

  static Future<List<Widget>> fetchAll() async{
    await CategoriesLoader.getCategories().then((value) => categories = value);
    List<Row> rows = [];
    if(categories == null || categories.length == 0) return rows;

    for(int i = 0; i < categories.length; i+=howMany){
      Row r;
      if(i+howMany-1<categories.length){
        r = Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: categories.getRange(i, i+howMany).toList(),
        );
        rows.add(r);
      }
      else{
        int howMuch = howMany;
        while(i+howMuch-1>=categories.length) howMuch--;
        r = Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: categories.getRange(i, i+howMuch).toList(),
        );
        rows.add(r);
        break;
      }
    }
    return rows;
  }
}

class Category extends StatelessWidget{
  final String _name;
  final String _gif;
  final Color _c;
  static GalleryState state;
  Category(this._name, this._gif, this._c);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => categoryTap(context, _name, _gif, _c),
      onLongPress: () => categoryLongTap(context, _name, _gif, _c),
      child: Container(
          height: 150,
          width: 110,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 90,
                width: 90,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(999),
                    border: Border.all(color: _c, width: 1)
                ),
                child: ClipOval(
                    child: Image.asset(gifPath+_gif+".gif",fit: BoxFit.fill,)
                ),
              ),
              Text(_name, style: TextStyle(color: Colors_peach, fontSize: 24),),
            ],
          )
      ),
    );
  }
  static Future<void> categoryTap(BuildContext context, String name, String gif, Color c) async {
    if(MyApp.vibrations) Vibration.vibrate(duration: 50);
    await Navigator.pushNamed(context, CategoryRoute, arguments: {'name': name, 'gif': gif, 'color': c });
    state.reload();
  }
  static Future<void> categoryLongTap(BuildContext context, String name, String gif, Color c) async {
    if(MyApp.vibrations) Vibration.vibrate(duration: 70);
    await Navigator.pushNamed(context, CreateEditRoute, arguments: {'name': name, 'gif': gif, 'color': c });
    state.reload();
  }
}