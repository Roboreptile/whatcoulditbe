import 'dart:io' as io;

import 'package:file/local.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:what_could_it_be/Widgets/images.dart';

class ImagesLoader{
  static io.Directory dir;
  static LocalFileSystem localFileSystem;
  static String pathTarget;
  static io.File targetFile;
  static List<String> imagesData;

  static Future<void> load(String name) async {

    if(dir==null){
      dir = await getApplicationDocumentsDirectory();
      localFileSystem = LocalFileSystem();
    }
    pathTarget = dir.path + "/category_"+name+".txt";
    targetFile = localFileSystem.file(pathTarget);
    print(pathTarget);
      imagesData = [];
      String allImages = await targetFile.readAsString();
      print("AllImages: "+allImages);
      if(allImages!= null && allImages != "") imagesData = allImages.split('\n');


    /*
      print(e);
      await targetFile.writeAsString("", mode: io.FileMode.write);
      imagesData = [];
    */
  }

  static Future<List<Widget>> getImages(String name) async{
    List<Widget> result = [];
    await load(name);
    print(imagesData);
    if(imagesData == null || imagesData.length == 0) return [];
    for(int i = 0; i< imagesData.length; i+=2) {
      String path = imagesData[i];
      if(path==null || path =="") break;
      List<String> data = imagesData[i+1].split("_");
      if(path == "/default") path = null;
      ImageBox b = ImageBox(data[0],path,int.parse(data[1]));
      result.add(b);
    }
    return result;
  }

  static Future<void> add(String name, String path, int stars) async{
    await targetFile.writeAsString(path+"\n", mode: io.FileMode.append).then((value) => targetFile.writeAsString(name+"_"+stars.toString()+"\n", mode: io.FileMode.append));
  }

  static Future<void> edit(String oldName, String name, String newPath, int stars) async{
    await targetFile.writeAsString("");
    for(int i = 0; i< imagesData.length; i+=2) {
      String path = imagesData[i];
      if(path==null || path =="") break;
      List<String> data = imagesData[i+1].split("_");

      if(oldName == data[0]){
        imagesData[i] = newPath;
        imagesData[i+1] = name+"_"+stars.toString();
        await targetFile.writeAsString(imagesData[i]+"\n", mode: io.FileMode.append);
        await targetFile.writeAsString(imagesData[i+1]+"\n", mode: io.FileMode.append);
      }
      else{
        await targetFile.writeAsString(imagesData[i]+"\n", mode: io.FileMode.append);
        await targetFile.writeAsString(imagesData[i+1]+"\n", mode: io.FileMode.append);
      }
    }
  }

  static Future<void> remove(String name) async{
    await targetFile.writeAsString("");
    for (int i = 0; i < imagesData.length; i += 2) {
      String path = imagesData[i];
      if (path == null || path == "") break;
      List<String> data = imagesData[i + 1].split("_");
      if(data[0]!=name) {
        await targetFile.writeAsString(imagesData[i]+"\n", mode: io.FileMode.append);
        await targetFile.writeAsString(imagesData[i+1]+"\n", mode: io.FileMode.append);
      }
    }
    return;
  }
  static Future<bool> test(String testName) async {
    for (int i = 0; i < imagesData.length; i += 2) {
      String path = imagesData[i];
      if (path == null || path == "") break;
      List<String> data = imagesData[i + 1].split("_");
      if (testName == data[0]) return true;
    }
    return false;
  }

  static int starCount(){
    int result = 0;
    for (int i = 0; i < imagesData.length; i += 2) {
      String path = imagesData[i];
      if (path == null || path == "") break;
      List<String> data = imagesData[i + 1].split("_");
      result += int.parse(data[1]);
    }
    return result;
  }
  static String getStar(int starry){
    if(starry == 0) return "/default\nDodaj zdjęcia :P";

    for (int i = 0; i < imagesData.length; i += 2) {
      String path = imagesData[i];
      if (path == null || path == "") break;
      List<String> data = imagesData[i + 1].split("_");
      starry -= int.parse(data[1]);
      if(starry<=0) return path+"\n"+data[0];
    }
    return "/default\nError :o";
  }
}