import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';
import 'package:what_could_it_be/Screens/category_screen.dart';
import 'package:what_could_it_be/constants.dart';
import '../Screens/category_screen.dart';
import '../main.dart';
import 'images_loader.dart';


class Images{
  static int howMany = 2;
  static List<Widget> images = [];

  static Future<List<Row>> fetchAll(String name) async{
    await ImagesLoader.getImages(name).then((value) => images = value);
    List<Row> rows = [];
    if(images == null || images.length == 0) return rows;
    for(int i = 0; i < images.length; i+=howMany){
      Row r;
      if(i+howMany-1<images.length){
        r = Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: images.getRange(i, i+howMany).toList(),
        );
        rows.add(r);
      }
      else{
        int howMuch = howMany;
        while(i+howMuch-1>=images.length) howMuch--;
        r = Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: images.getRange(i, i+howMuch).toList(),
        );
        rows.add(r);
        break;
      }
    }
    return rows;
  }
}

class ImageBox extends StatelessWidget{
  final String _name;
  final String _path;
  final int _stars;
  final io.File _f;
  final Row stars;

  static Color c = Colors.black;
  static CategoryState state;
  ImageBox(this._name,this._path,this._stars):_f = _path!= null ? io.File(_path) : null, stars = makeStars(_stars);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
      width: MediaQuery.of(context).size.width/2,
      height: MediaQuery.of(context).size.width/2,
      alignment: Alignment.bottomCenter,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: _f != null ? FileImage(_f) : AssetImage(imgPath+'missing.png'),
          fit: BoxFit.cover,
        ),

      ),
      child: Container(
          height: 30,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: c.withOpacity(0.5),
          ),
          child: Table(
            children: <TableRow>[
              TableRow(
                children: [
                  Container(
                    height: 30,
                    padding: EdgeInsets.symmetric(horizontal: 5 ),
                    alignment: Alignment.centerLeft,
                    child: Text(_name,
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.white)
                    ),
                  ),
                  stars,
                ],
              )
            ]
          )
      ),
        ),
      onLongPress: () => editImage(context, _name, _path, _stars),
    );
  }

  static Row makeStars(int stars) {
    List<Widget> result = [];
    for(int i = 0; i< 5; i++){
      result.add(Container(
        width: 12, height: 30,
        color: Colors.transparent,
        child: Icon(Icons.star, color: i<stars ? Colors.amber : Colors.blueGrey),
      ));
    }
    return Row(
        children: result,
      );
  }

  static Future<void> editImage(BuildContext context, String name, String path, int stars) async{
    if(path == null) path = '/default';
    if(MyApp.vibrations) Vibration.vibrate(duration: 70);
    await Navigator.pushNamed(context, CreateImageEditRoute, arguments: {'name':name,'path':path,'stars':stars, 'color': CategoryState.c});
    state.reload();
  }
}