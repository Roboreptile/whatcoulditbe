import 'dart:io' as io;

import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:file/local.dart';

import '../Screens/gallery_screen.dart';
import 'categories.dart';


class CategoriesLoader{
  static io.Directory dir;
  static String pathMain;
  static LocalFileSystem localFileSystem;
  static io.File mainFile;
  static List<String> categories;

  static Future<void> load() async{
    if(dir==null){
      dir = await getApplicationDocumentsDirectory();
      pathMain = dir.path + "/category.txt";
      localFileSystem = LocalFileSystem();
    }
    mainFile = localFileSystem.file(pathMain);
    print(pathMain);
    try{
      categories = [];
      String allCategories = await mainFile.readAsString();
      print(allCategories);
      categories = allCategories.split('\n');
      if(categories==null || categories.length == 0) categories = [];
    }
    catch(e){
      print(e.toString());
      await mainFile.writeAsString("", mode: io.FileMode.write);
      categories = [];
    }
  }

  static Future<List<Widget>> getCategories() async{
    await load();
    List<Category> result = [];
    if (categories == null || categories.length==0) return result;
    for (String string in categories) {
      List<String> data = string.split("_");
      if (data.length != 5) break;
      Color c = Color.fromRGBO(int.parse(data[2]), int.parse(data[3]), int.parse(data[4]), 1);
      result.add(Category(data[0], data[1], c));
    }
    return result;
  }

  static Future<void> addCategory(String name, String gifName, Color allColor) async{
    io.File categoryFile = localFileSystem.file(dir.path + "/category_"+name+".txt");
    categories.add(name+"_"+gifName+"_"+allColor.red.toString()+"_"+allColor.green.toString()+"_"+allColor.blue.toString());
    await mainFile.writeAsString(name+"_"+gifName+"_"+allColor.red.toString()+"_"+allColor.green.toString()+"_"+allColor.blue.toString()+"\n", mode: io.FileMode.append); //TODO CHANGE TO APPEND!!!!
    await categoryFile.writeAsString("");
    return;
  }

  static Future<bool> testForCategory(String name) async{
    for(String string in categories) {
      List<String> data = string.split("_");
      if (data[0] == name) return true;
    }
    return false;
  }

  static Future<void> clear({GalleryState state}) async {
    for (String string in categories) {
      List<String> data = string.split("_");
      if(data[0]==null || data[0]=="") break;
      io.File tmp = localFileSystem.file(dir.path + "/category_" + data[0] + ".txt");
      tmp.delete();
    }
    await mainFile.writeAsString("");
    state.reload();
  }

  static Future<void> remove(name) async{
    for(int i = 0; i<categories.length; i++){
      List<String> data = categories[i].split("_");
      if(data[0]==null || data[0]=="") break;
      if(data[0] == name){
        categories.removeAt(i);
        io.File tmp = localFileSystem.file(dir.path + "/category_" + data[0] + ".txt");
        tmp.delete();
        break;
      }
    }
    await mainFile.writeAsString(categories.join("\n"));
  }

  static Future<void> edit(String oldName, String newName,  String newGif, Color newColor) async{
    if(oldName!=newName){
      io.File tmp = localFileSystem.file(dir.path + "/category_" + oldName + ".txt");
      String data =  await tmp.readAsString();
      await localFileSystem.file(dir.path + "/category_" + newName + ".txt").writeAsString(data);
      tmp.delete();
    }

    for(int i = 0; i<categories.length; i++){
      List<String> data = categories[i].split("_");
      if(data[0]==null || data[0]=="") break;
      if(data[0] == oldName){
        data[0] = newName;
        data[1] = newGif;
        data[2] = newColor.red.toString();
        data[3] = newColor.green.toString();
        data[4] = newColor.blue.toString();
        categories[i] = data.join("_");
        break;
      }
    }

    await mainFile.writeAsString(categories.join("\n"));
  }
}