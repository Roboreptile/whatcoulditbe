import 'package:flutter/material.dart';

// ROUTES
const GalleryRoute = '/';
const CategoryRoute = '/category';

const CreateRoute = '/create';
const CreateEditRoute = '/create_edit';

const CreateImageRoute = '/category_image';
const CreateImageEditRoute = '/category_image_edit';

const WinnersRoute = "/victory";

// COLORS
const Color Colors_claret = Color.fromRGBO(127, 23, 52, 1);
const Color Colors_turfs_blue = Color.fromRGBO(1, 151, 246, 1);

const Color Colors_blue_rib = Color.fromRGBO(71, 76, 255, 1);
const Color Colors_indigo = Color.fromRGBO(73, 0, 158, 1);
const Color Colors_peach = Color.fromRGBO(250, 219, 158, 1);
const Color Colors_max_yellow_red = Color.fromRGBO(245, 184, 65, 1);

const String imgPath = 'assets/images/';
const String gifPath = 'assets/gifs/';

class CGen{
  static Color colorGenerator(Color c, int idx){
    if(cEq(c, Colors.red)){//
      if(idx == 1) return cs("ff4646");  //Text & Button
      if(idx == 2) return cs("ffb396");  //Text bg
      if(idx == 3) return cs("ff8585");  //Bg
      if(idx == 4) return cs("17BEBB");  //CreateScreen
    }
    else if(cEq(c, Colors.green)){//
      if(idx == 1) return cs("184d47");
      if(idx == 2) return cs("96bb7c");
      if(idx == 3) return cs("d6efc7");
      if(idx == 4) return cs("fad586");

    }
    else if(cEq(c, Colors.blue)){
      if(idx == 1) return cs("7579e7");
      if(idx == 2) return cs("9ab3f5");
      if(idx == 3) return cs("b9fffc");
      if(idx == 4) return cs("b9fffc");

    }
    else if(cEq(c, Colors.purpleAccent)){//
      if(idx == 1) return cs("52057b");
      if(idx == 2) return cs("892cdc");
      if(idx == 3) return cs("bc6ff1");
      if(idx == 4) return cs("bc6ff1");

    }
    else if(cEq(c, Colors_claret)){//
      if(idx == 1) return cs("cf1b1b");
      if(idx == 2) return cs("900d0d");
      if(idx == 3) return cs("000000");
      if(idx == 4) return cs("ffdbc5");
    }
    else if(cEq(c, Colors.black)){//
      if(idx == 1) return cs("000000");
      if(idx == 2) return cs("000000").withOpacity(0.1);
      if(idx == 3) return cs("f6f6f6");
      if(idx == 4) return cs("a34a28");

    }
    else if(cEq(c, Colors.yellow)){//
      if(idx == 1) return cs("fca652");
      if(idx == 2) return cs("ffefa0");
      if(idx == 3) return cs("fca652");
      if(idx == 4) return cs("ac4b1c");
    }
    else if(cEq(c, Colors.orange)){//
      if(idx == 1) return cs("e27802");
      if(idx == 2) return cs("f88f01");
      if(idx == 3) return cs("ec4646");
      if(idx == 4) return cs("58391c");
    }
    return Colors.black;
  }

  static Widget bgGenerator(BuildContext context, Color c){
    String path = 'assets/bgs/';
    if(cEq(c, Colors.red)) path += 'red4.jfif';
    else if(cEq(c, Colors.green)) path += 'green.jfif';
    else if(cEq(c, Colors.blue)) path += 'blue.jpg';
    else if(cEq(c, Colors.purpleAccent)) path += 'purple2.jpg';
    else if(cEq(c, Colors_claret)) path += 'claret.jpg';
    else if(cEq(c, Colors.black)) path += 'black.jfif';
    else if(cEq(c, Colors.yellow)) path += 'yellow2.jpg';
    else if(cEq(c, Colors.orange)) path += 'orange2.jfif';

    return Image.asset(path,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      fit: BoxFit.cover,
    );
  }

  static bool cEq(Color c, Color c2){
    return c.red == c2.red && c.blue == c2.blue && c.green == c2.green;
  }
  
  static Color cs(String code) {
    return Color(int.parse(code.substring(0, 6), radix: 16) + 0xFF000000);
  }

}