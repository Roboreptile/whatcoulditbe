import 'dart:ui';
import 'dart:io' as io;

import 'package:permission_handler/permission_handler.dart';
import 'package:path_provider/path_provider.dart';
import 'package:file/local.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';

import '../Widgets/categories_loader.dart';
import '../Widgets/categories.dart';
import '../main.dart';
import 'gallery_screen.dart';

class Settings extends StatefulWidget{
  final GalleryState galleryState;
  Settings(this.galleryState);

  @override
  SettingsState createState() => SettingsState(this.galleryState);

  static List<String> settings;
  static io.File mainFile;
  static loadSettings() async{
    io.Directory dir = await getApplicationDocumentsDirectory();
    String pathMain = dir.path + "/settings.txt";
    LocalFileSystem localFileSystem = LocalFileSystem();
    mainFile = localFileSystem.file(pathMain);

    try{
      String data = await mainFile.readAsString();
      settings = data.split("_");
      if(settings == null || settings.length == 0){
        mainFile.writeAsString("3_true");
        settings = ["3","true"];
      }
    }
    catch(e){
      mainFile.writeAsString("3_true");
      settings = ["3","true"];
    }
    update();
  }
  static update(){
    Categories.howMany = int.parse(settings[0]);
    MyApp.vibrations = settings[1] == "true";
    mainFile.writeAsString(settings.join("_"));
    print(settings.join("_"));
  }
}

class SettingsState extends State<Settings>{
  final GalleryState galleryState;

  SettingsState(this.galleryState);

  TextStyle settingsStyle(){
    return TextStyle(
      fontFamily: 'Caveat',
      fontSize: 12,

    );
  }
  TextStyle mainSettingsStyle(){
    return TextStyle(
      fontFamily: 'Arial',
      fontSize: 13,
    );
  }
  TextStyle categorySettingsStyle(){
    return TextStyle(
      fontFamily: 'Arial',
      fontSize: 15,
      decoration: TextDecoration.underline
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        elevation: 16,
        child: Container(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: [
              Container(height: 60, alignment: Alignment.center, child: Text("Ustawienia", style: TextStyle(fontSize: 20),),),
              setting("Kategorie/Rząd", comment: "Aktualnie: "+Categories.howMany.toString(), w:
                Slider(
                  value: Categories.howMany/1.0, min: 1.0, max: 3.0,
                  activeColor: Colors.red, inactiveColor: Colors.red.withOpacity(0.5),
                  divisions: 2,
                  onChanged: (newVal) => {
                    Categories.howMany = newVal.floor(),
                    Settings.settings[0] = Categories.howMany.toString(),
                    Settings.update(),
                    setState((){}),
                    galleryState.reload(),
                  })),
              setting("Wibracje", comment:"Wibruj przy naciśnięciu na guziki" ,w:
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Switch(value: MyApp.vibrations, onChanged: (value)=>{
                    MyApp.vibrations = value, if(MyApp.vibrations){Vibration.vibrate(duration: 50)},
                    Settings.settings[1] = value.toString(),
                    Settings.update(),
                    setState((){}),
                  },
                  activeColor: Colors.red, activeTrackColor: Colors.red.withOpacity(0.85), inactiveTrackColor: Colors.red.withOpacity(0.3), inactiveThumbColor: Colors.red, )
                ),
              ),
              setting("Fix-all love buttons:"),
              setting('Daj uprawnienia: ', comment:"Nie działa, gdy są już nadane", w: heartGesture(() {Permission.storage.request(); if(MyApp.vibrations) Vibration.vibrate(duration: 50);})),
              setting('Odśwież kategorie', comment:"W razie, gdy sobie zniknęły",w: heartGesture(() {galleryState.reload(); if(MyApp.vibrations) Vibration.vibrate(duration: 50);})),
              setting("Usuń dane", comment: "Przytrzymaj przycisk", w: GestureDetector(
                child: Container(
                  decoration: BoxDecoration(
                    //border: Border.all(),
                    color: Colors.transparent,
                  ),
                  height: 30,
                  width: 30,
                  alignment: Alignment.topCenter,
                  child: Icon(Icons.favorite, color: Colors.red),
                ),
                onLongPress: () => _showMyDialog(),
                )
              ),
            ],
          ),
        )
    );
  }

  Widget setting(String message,{String comment, Widget w}){
    if(comment==null) comment="";
    if(w!=null){
      return Container(
        height: 50,
        alignment: Alignment.center,
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.top,
          children: [
            TableRow(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    Text(message, style: mainSettingsStyle()),
                    Text(comment, style: settingsStyle())
                  ],),
                  w,
                ]
            )
          ],
        ),
      );
    }
    return Container(
      height: 50,
      alignment: Alignment.centerLeft,
      child: Text(message, style: categorySettingsStyle()),
    );
  }
  Widget heartGesture(void Function() f){
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          //border: Border.all(),
          color: Colors.transparent,
        ),
        height: 30,
        width: 30,
        alignment: Alignment.topCenter,
        child: Icon(Icons.favorite, color: Colors.red),
      ),
      onTap: f,
    );
  }
  Future<void> _showMyDialog() async {
    if(MyApp.vibrations) Vibration.vibrate(duration: 50);
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Na pewno?'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Trzeba będzie od nowa dodawać wszystkie kategorie!'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Yup'),
              onPressed: () {
                CategoriesLoader.clear(state: galleryState);
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Nope'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}