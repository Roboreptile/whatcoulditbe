import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';
import '../Widgets/images_loader.dart';
import '../Widgets/images.dart';
import '../constants.dart';
import '../main.dart';

class CategoryScreen extends StatefulWidget{
  final String _name;
  final String _gif;
  final Color _c;
  CategoryScreen(this._name, this._gif, this._c);

  @override
  CategoryState createState() => CategoryState(_name,_gif,_c);
}

class CategoryState extends State<CategoryScreen>{
  final String _name;
  final String _gif;
  static Color c;
  List<Row> images = [];
  static Color main = Colors.black;
  static Color secondary = Colors.black;
  static Color third = Colors.black;

  CategoryState(this._name, this._gif, Color c){
    ImageBox.state = this;
    CategoryState.c = c;
    main = CGen.colorGenerator(c,1);
    secondary = CGen.colorGenerator(c,2);
    third = CGen.colorGenerator(c,3);
    ImageBox.c = c;
    images = [];
    reload();
  }

  FutureOr reload() { Images.fetchAll(_name).then((value) => images = value).then((value) => setState(() {}), onError: null);}

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CGen.bgGenerator(context, c),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: ListView(
          children: [
            Container(
            height: 100,
            alignment: Alignment.center,
            child: Container(
                height: 90,
                width: MediaQuery.of(context).size.width-10,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: main, width: 2),
                  color: secondary,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ClipOval(
                      child: Container(
                        width: 80,
                        height: 80,
                        child: Image.asset(gifPath+_gif+".gif", fit: BoxFit.fill,),
                      ),
                    ),
                    Text(
                      _name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 45,
                        color: third,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    ClipOval(
                      child: Container(
                        width: 80,
                        height: 80,
                        child: Image.asset(gifPath+_gif+".gif", fit: BoxFit.fill,),
                      ),
                    ),
                  ],
                )
              ),
            ),
            Container(
              height: 40,
              alignment: Alignment.center,
              child: GestureDetector(
              onTap: () => createImageTap(context),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.green),
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.green.withOpacity(0.2),
                ),
                child: Text("Dodaj nowe zdjęcie", style: TextStyle(color: Colors.green, fontSize: 23, fontWeight: FontWeight.w900)),
              ),
              ),
            ),
            Container(height: 5,),
            ]..addAll(images)..add(Container(height: 50,)),
            ),
            floatingActionButton: FlatButton(
              child: Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(color: main,width: 3),
                  color: Colors.transparent,
                ),
                child: Image.asset(gifPath+"cube.gif"),
              ),
              onPressed: () => winningTap(context),
              ),
          ),
      ],
    );
  }
  Future<void> createImageTap(BuildContext context) async{
    if(MyApp.vibrations) Vibration.vibrate(duration: 50);
    await Navigator.pushNamed(context, CreateImageRoute, arguments: {'color': c});
    reload();
  }

  Future<void> winningTap(BuildContext context) async{
    if(MyApp.vibrations) Vibration.vibrate(duration: 50);
    var rng = new Random();
    int starCount = ImagesLoader.starCount();
    int starry;
    if(starCount == 0) starry = 0;
    else starry = rng.nextInt(starCount)+1;
    String winner = ImagesLoader.getStar(starry);
    String winnersName = winner.split("\n")[1];
    String winnersPath = winner.split("\n")[0];
    await Navigator.pushNamed(context, WinnersRoute, arguments: {'name':winnersName, "path": winnersPath, 'color': c});
  }
}