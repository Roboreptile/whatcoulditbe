import 'dart:async';

import 'package:permission_handler/permission_handler.dart';
import 'package:vibration/vibration.dart';
import 'package:flutter/material.dart';



import '../Widgets/categories_loader.dart';
import '../Widgets/categories.dart';
import '../constants.dart';
import '../main.dart';
import 'settings_screen.dart';


class GalleryScreen extends StatefulWidget{
  @override
  GalleryState createState() => GalleryState();
}

class GalleryState extends State<GalleryScreen>{
  List<Row> categories = [];

  GalleryState(){
    CategoriesLoader();
    Settings.loadSettings();
    Category.state = this;
    Permission.storage.request().then(reload());
  }

  FutureOr reload(){
    Categories.fetchAll().then((value) => categories = value).then((value) => setState((){}),onError: null);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Settings(this),
      body:  Container(
        decoration: BoxDecoration(
            gradient: RadialGradient(
              center: Alignment.bottomRight,
              radius: 1.5,
              colors: [Colors_blue_rib, Colors_indigo]
            )
        ),
        child: ListView(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:[
                Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(gifPath+'cube.gif',width: 100,height: 100,),
                        Image.asset(imgPath+'logo.png',width: 100, height:100,),
                        Image.asset(gifPath+'cube.gif',width: 100,height: 100,),
                      ]),
                  ),
                ],
              ),
            Container(
              height: 80,
              alignment: Alignment.centerRight,
              child: Stack(
                alignment: Alignment.center,
                children: [Container(
                  height: 10,
                  color: Colors_max_yellow_red,
                ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(imgPath+'flower.png', fit: BoxFit.fill,),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(imgPath+'flower.png', fit: BoxFit.fill,),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(imgPath+'flower.png', fit: BoxFit.fill,),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(width: 0, height: 10,)
          ]..addAll(categories)..add(Container(height: 100, width: 0,)),
        )
      ),
      floatingActionButton: FlatButton(
        child: Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              border: Border.all(color: Colors_max_yellow_red,width: 1.5),
              color: Colors_max_yellow_red,
          ),
          child:Icon(
            Icons.add,
            size: 50,
            color: Colors.white,
          ),
        ),
        onPressed: () => {
          if(MyApp.vibrations) Vibration.vibrate(duration: 50),
          locationTap(context)
        },
      ),
    );
  }

  Future<void> locationTap(BuildContext context) async {
    await Navigator.pushNamed(context, CreateRoute);
    reload();
  }
}