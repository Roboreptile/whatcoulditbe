import 'dart:io' as io;
import 'dart:math';

import 'package:flutter/material.dart';
import '../constants.dart';

class VictoryScreen extends StatefulWidget{

  final String _path;
  final String _name;
  final Color _c;
  VictoryScreen(this._name, this._path, this._c);
  @override
  VictoryState createState() => VictoryState(this._name, this._path, this._c);
}

class VictoryState extends State<VictoryScreen>{
  static String _path;
  static String _name;
  static Color _c;
  VictoryState(String name, String path, Color c){
    _name = name; _path = path; _c = c;
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Stack(
        children: [
          CGen.bgGenerator(context, _c),
        Material(
          type: MaterialType.transparency,
          child: Container(
            height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
            child: Container(
              height: 500,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: CGen.colorGenerator(_c, 1)),
                borderRadius: BorderRadius.circular(10),
                color: CGen.colorGenerator(_c, 2),
              ),
              child:  Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 100,
                    child: Text(randomText(), style: TextStyle(fontSize: 40, fontWeight: FontWeight.w900, color: CGen.colorGenerator(_c, 3)),),
                  ),
                  Container(
                    width: 200, height: 200,
                    child: _path!="/default" ? Image.file(io.File(_path), fit: BoxFit.cover) : Image.asset(imgPath+"missing.png", fit: BoxFit.cover),
                  ),
                  Container(
                    height: 50,
                    alignment: Alignment.center,
                    child: Text(_name+"!", style: TextStyle(fontSize: 30, color: CGen.colorGenerator(_c, 3)),),
                  ),
                  Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    child: randomGif(),
                  )
                ],
              ),
            )

          ),
        ),
        ],
        )
      );
  }

  String randomText(){
    var rng = new Random();
    List<String> vals = [
      "Dziś czas na...",
      "Wygrał...",
      "Wypadło...",
      "Dzisiaj...",
      "Hooray!",
      "Teraz...",
      "A więc..."
    ];
    return vals[rng.nextInt(vals.length)];
  }
  Widget randomGif(){
    var rng = new Random();
    int idx = rng.nextInt(11)+1;
    return Container(
      height: 150,
      width: 150,
      child: Image.asset('assets/celebrate/'+idx.toString()+".gif", fit: BoxFit.contain,),
    );
  }

}