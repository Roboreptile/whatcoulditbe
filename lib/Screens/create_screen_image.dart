import 'dart:ui';
import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vibration/vibration.dart';
import '../Widgets/images_loader.dart';
import '../constants.dart';
import '../main.dart';


class CreateImageScreen extends StatefulWidget{
  final String _name;
  final String _path;
  final int _stars;
  final Color _c;
  CreateImageScreen(this._name,this._path,this._stars, this._c);
  @override
  CreateImageState createState() => CreateImageState(this._name,this._path,this._stars, this._c);
}

class CreateImageState extends State<CreateImageScreen>{
  static String _name;
  static String _path;
  static int _stars;
  static String _mode;
  static Color _c;
  static io.File _f;
  static TextEditingController boxName = TextEditingController();
  final String originalName;
  static Color _buttonColor;
  CreateImageState(String name, String path, int stars, Color c): originalName = name{
    if(name!=null){
      _mode = "Edytuj";
      _name = name;
      _path = path;
      _stars = stars;
      _c = c;
      _f = path!="/default" ? io.File(_path) : null;
      boxName = TextEditingController();
      boxName.text = _name;
      _buttonColor = CGen.colorGenerator(c, 4);
    }
    else{
      _mode = "Nowe Zdjęcie";
      _name = null;
      _path = null;
      _stars = 3;
      _c = c;
      _f = null;
      boxName = TextEditingController();
      _buttonColor = CGen.colorGenerator(c, 4);
    }
  }

  void dispose(){
    boxName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [
        CGen.bgGenerator(context, _c),
        Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView(
        children: [
          Container(
              height: 80,
              alignment: Alignment.center,
              child: Text(_mode, style: TextStyle(color: CGen.colorGenerator(_c, 4), fontSize: 35, fontWeight: FontWeight.bold, ),)
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(alignment: Alignment.center, height: 100, width: 100, child: Text("Nazwa:\n ", style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold,  color: _buttonColor)), ),
              Container(alignment: Alignment.center, height: 100, width: 200,
                child: TextField(
                  textCapitalization: TextCapitalization.words,
                  maxLength: 14,
                  maxLengthEnforced: true,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    enabledBorder: InputBorder.none, border: InputBorder.none, focusedBorder: InputBorder.none,
                  ),
                  controller: boxName,
                  selectionWidthStyle: BoxWidthStyle.tight,
                ),
              )
            ],
          ),
          Row(
            children: [
              Container(alignment: Alignment.center, height: 100, width: 100, child: Text("Gwiazdki: ", style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: CGen.colorGenerator(_c, 4))),),
            ]..addAll([Container(width:20),star(1),star(2),star(3),star(4),star(5)]),
          ),
          Container(height: 40, alignment: Alignment.center, child: Text("Zdjęcie", style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold,  color: CGen.colorGenerator(_c, 4))),),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                child: Container(width: 150, height: 150,
                decoration: BoxDecoration(
                border: Border.all(),
                ),
                child: _f!=null ? Image.file(_f, fit: BoxFit.cover,) : Image.asset(imgPath+"missing.png"),
                ),
                onTap: () => getNewImage(),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  width: 150,
                  height: 30,
                  color: Colors.green,
                  child: Text("Zamień/Załaduj", style: TextStyle(fontSize: 16, color: Colors.black),),
                ),
                onTap: () => getNewImage(),
              ),
            ],
          ),
          Container(height:100),
          _mode != "Edytuj" ? Container(height: 50) : GestureDetector(
            child: Container(
              height: 100,
              color: Colors.red,
              alignment: Alignment.center,
              child: Text("Usuń kategorię", style: TextStyle(color: Colors.white, fontSize: 30),),
            ),
            onTap: () => _showMyDialog(),
          )
        ],
      ),
      floatingActionButton: FlatButton(
        child: Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            border: Border.all(color: _buttonColor,width: 1.5),
            color: _buttonColor,
          ),
          child:Icon(
            _mode == "Nowe Zdjęcie" ? Icons.add : Icons.save,
            size: 50,
            color: Colors.white,
          ),
        ),
        onPressed: () => addNewImage(),
      ),
    )],
    );
  }

  Widget star(int id){
    return GestureDetector(
      onTap: () => {
        _stars = id,
        setState((){})
      },
      child: Container(
        width: 30, height: 30,
        color: Colors.transparent,
        child: Icon(Icons.star, color: id<=_stars ? Colors.amber : Colors.blueGrey, size: 30,),
      ),
    );
  }

  void getNewImage() async{
    var img = await ImagePicker().getImage(source: ImageSource.gallery);
    if(img.path!=null&&img.path!="") {
      _path = img.path;
      setState(() {});
      CreateImageState._f = io.File(img.path);
    }
  }

  void addNewImage() async{
    _name = boxName.text;
    if(_name==null || _name==""){
      flashButtonRed();
      if(MyApp.vibrations) Vibration.vibrate(pattern: [0,50,200,50]);
      return;
    }
    if(_path == null || _path == "") _path = "/default";
    if(_mode!="Edytuj"){
      if(await ImagesLoader.test(_name)==true){
        flashButtonRed();
        if(MyApp.vibrations) Vibration.vibrate(pattern: [0,50,200,50]);
        return;
      }
      else await ImagesLoader.add(_name,_path,_stars);
    }
    else await ImagesLoader.edit(originalName, _name,_path, _stars);
    Navigator.pop(context);
  }

  void flashButtonRed() async {
    Color tmp = _buttonColor;
    _buttonColor = Colors.red;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () =>
    {
      _buttonColor = tmp,
      setState(() {})
    });
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Na pewno?'),
          actions: <Widget>[
            TextButton(
              child: Text('Yup'),
              onPressed: () async{
                await ImagesLoader.remove(originalName);
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Nope'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}