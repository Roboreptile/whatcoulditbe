import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';


import '../Widgets/categories_loader.dart';
import '../Widgets/pickers.dart';
import '../constants.dart';
import '../main.dart';

class CreateScreen extends StatefulWidget{
  final String _name;
  final String _gif;
  final Color _c;

  CreateScreen(this._name, this._gif, this._c);

  @override
  CreateState createState(){
    if(_name == null) return CreateState();
    return CreateState.edit(_name, _gif, _c);
  }
}

class CreateState extends State<CreateScreen>{
  static String mode = "Nowa Kategoria";
  static TextEditingController boxName = TextEditingController();
  static Color allColor = Colors.red;
  static String gifName = "";
  static String originalName;

  Color _buttonColor = Colors_max_yellow_red;

  CreateState(){
    mode = "Nowa Kategoria";
    gifName = "";
    allColor = Colors.red;
    boxName = TextEditingController();
    originalName = null;
  }
  CreateState.edit(String name, String gif, Color c){
    mode = "Edytuj";
    boxName = TextEditingController();
    boxName.text = name;
    gifName = gif;
    allColor = c;
    originalName = name;
  }

  void reload() => setState((){});

  void dispose(){
    boxName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: RadialGradient(
              center: Alignment.bottomRight,
              radius: 1.5,
              colors: [Colors_blue_rib, Colors_indigo]
          ),
        ),
        child: ListView(
          children: [
            Container(alignment: Alignment.center, height: 100, child: Text(mode, style: TextStyle(fontSize: 40, color: Colors_max_yellow_red),),),
            Row(
              children: [
                Container(alignment: Alignment.center, height: 100, width: 100, child: Text("Nazwa: \n", style: TextStyle(fontSize: 20, color: Colors_max_yellow_red)),),
                Container(alignment: Alignment.center, height: 100, width: 200,
                  child: TextField(
                    textCapitalization: TextCapitalization.words,
                    maxLength: 10,
                    maxLengthEnforced: true,
                    style: TextStyle(color: _buttonColor),
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.black38.withOpacity(0.1),
                        enabledBorder: InputBorder.none, border: InputBorder.none, focusedBorder: InputBorder.none,
                    ),
                    controller: boxName,
                    selectionWidthStyle: BoxWidthStyle.tight,
                  ),
                )
              ],
            ),
            Row(
              children: [
                Container(alignment: Alignment.center, height: 50, width: 90, child: Text("Styl: ", style: TextStyle(fontSize: 20, color: Colors_max_yellow_red)),),
                ColorPicker(this),
              ],
            ),
            Container(
              height: 100,
              alignment: Alignment.center,
              child: Text("Rysunek: ", style: TextStyle(fontSize: 20, color: Colors_max_yellow_red)),
            ),
            GifPicker(this),
            mode == "Nowa Kategoria" ? Container(height: 50,) : GestureDetector(
              child: Container(
                height: 100,
                color: Colors.red,
                alignment: Alignment.center,
                child: Text("Usuń kategorię", style: TextStyle(color: Colors.white, fontSize: 30),),
              ),
              onTap: () => _showMyDialog(),
            )
          ],
        ),
      ),
      floatingActionButton: FlatButton(
        child: Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            border: Border.all(color: _buttonColor,width: 1.5),
            color: _buttonColor,
          ),
          child:Icon(
            mode == "Nowa Kategoria" ? Icons.add : Icons.save,
            size: 50,
            color: Colors.white,
          ),
        ),
        onPressed: () => addNewCategory(),
      ),
    );
  }

  void addNewCategory() async{
    bool test = false;
    if(mode=="Nowa Kategoria") test = await CategoriesLoader.testForCategory(boxName.text.toString());
    else{
      if(originalName!=boxName.text.toString()) test = await CategoriesLoader.testForCategory(boxName.text.toString());
    }
    if(test) {
      if(MyApp.vibrations) Vibration.vibrate(pattern: [0,50,200,50]);
      flashButtonRed();
      return;
    }
    if(boxName.text==null || boxName.text=="" || gifName == "") return;
    if(mode=="Nowa Kategoria")await CategoriesLoader.addCategory(boxName.text.toString(), gifName, allColor);
    else await CategoriesLoader.edit(originalName,boxName.text.toString(), gifName, allColor);
    if(MyApp.vibrations) Vibration.vibrate(duration: 50);
    Navigator.pop(context);
  }

  void flashButtonRed() async{
    _buttonColor = Colors.red;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () => {
      _buttonColor = Colors_max_yellow_red,
      setState(() {})
    });

  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Na pewno?'),
          actions: <Widget>[
            TextButton(
              child: Text('Yup'),
              onPressed: () {
                CategoriesLoader.remove(originalName);
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Nope'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
