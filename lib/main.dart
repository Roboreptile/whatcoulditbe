import 'package:flutter/material.dart';


import 'Screens/category_screen.dart';
import 'Screens/create_screen.dart';
import 'Screens/gallery_screen.dart';
import 'Screens/create_screen_image.dart';
import 'Screens/victory_screen.dart';
import 'constants.dart';


Future<void> main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static bool vibrations = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: route(),
      theme: theme(),
    );
  }

  ThemeData theme() {
    return ThemeData(
      fontFamily: 'Caveat',
    );
  }

  RouteFactory route(){
    return (settings) {
      final Map<String, dynamic> arguments = settings.arguments;
      Widget screen;
      switch(settings.name){
        case GalleryRoute:
          screen = GalleryScreen();
          break;
        case CategoryRoute:
          screen = CategoryScreen(arguments['name'], arguments['gif'], arguments['color']);
          break;
        case CreateRoute:
          screen = CreateScreen(null,null,null);
          break;
        case CreateEditRoute:
          screen = CreateScreen(arguments['name'], arguments['gif'], arguments['color']);
          break;
        case CreateImageRoute:
          screen = CreateImageScreen(null,null,null, arguments['color']);
          break;
        case CreateImageEditRoute:
          screen = CreateImageScreen(arguments['name'], arguments['path'], arguments['stars'], arguments['color']);
          break;
        case WinnersRoute:
          screen = VictoryScreen(arguments['name'], arguments['path'], arguments['color']);
          break;
        default:
          screen = null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen );
    };
  }
}